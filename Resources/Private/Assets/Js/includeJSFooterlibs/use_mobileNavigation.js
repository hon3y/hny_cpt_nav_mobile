/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Andreas Hafner, Dominik Hilser, Georg Kathan, Hendrik Krüger, Timo Bittner :: teufels GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 **/
var hive_cpt_nav_mobile__interval = setInterval(function () {

    if (typeof jQuery == 'undefined') {
    }  else {

        if ( typeof hive_thm_bs__loaded != 'undefined'  && typeof hive_thm_bs__loaded == "boolean" && hive_thm_bs__loaded) {

            if (typeof tx_hive_cpt_nav_mobile__nav != 'undefined' && typeof tx_hive_cpt_nav_mobile__nav == "boolean" && tx_hive_cpt_nav_mobile__nav
            ) {

                if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
                    console.info('Nav - Mobile loaded');
                }

                clearInterval(hive_cpt_nav_mobile__interval);

                $('.hamburger ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
                    // Avoid following the href location when clicking
                    event.preventDefault();
                    // Avoid having the menu to close when clicking
                    event.stopPropagation();
                    // Re-add .open to parent sub-menu item

                    if ($(this).closest('li.dropdown').find('ul.dropdown-menu:first').hasClass('show')) {
                        $(this).closest('li.dropdown').find('ul.dropdown-menu:first').removeClass('show');
                        $(this).closest('li.dropdown').removeClass('show');
                    } else {
                        $(this).closest('li.dropdown').find('ul.dropdown-menu:first').addClass('show');
                        $(this).closest('li.dropdown').addClass('show');

                        /*$('li.active.current').parents('li.active.dropdown').map(function(){
                            $(this).addClass('show');
                        })*/
                      // $(".hamburger li.active").closest("ul.dropdown-menu").addClass("show");
                      // $(".hamburger li.active:not(.current)").addClass("show");
                    }

                });

                $('.hamburger button').on('click', function(event){

                    // Avoid following the href location when clicking
                    event.preventDefault();
                    // Avoid having the menu to close when clicking
                    event.stopPropagation();
                    // Re-add .open to parent sub-menu item
                    if ($(this).closest('div').find('ul.dropdown-menu:first').hasClass('show')) {
                        $(this).closest('div').find('ul.dropdown-menu:first').removeClass('show');
                    } else {

                        $(this).closest('div').find('ul.dropdown-menu:first').addClass('show');

                        /*$('li.active.current').parents('li.active.dropdown').map(function(){
                            $(this).addClass('show');
                        });*/
                      $(".hamburger li.active").closest("ul.dropdown-menu").addClass("show");
                      $(".hamburger li.active:not(.current)").addClass("show");
                    }

                });

            }

        }

    }

}, 1000);

var hive_cpt_nav_mobile_buttonHamburger__interval = setInterval(function () {

    if (typeof jQuery == 'undefined') {
    }  else {

        if ( typeof hive_thm_bs__loaded != 'undefined'  && typeof hive_thm_bs__loaded == "boolean" && hive_thm_bs__loaded) {

            if (typeof tx_hive_cpt_nav_mobile__nav != 'undefined'
                    && typeof tx_hive_cpt_nav_mobile__nav == "boolean"
                    && tx_hive_cpt_nav_mobile__nav
                    && typeof hive_cfg_typoscript__windowLoad != 'undefined'
                    && typeof hive_cfg_typoscript__windowLoad == "boolean"
                    &&  hive_cfg_typoscript__windowLoad
            ) {

                if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
                    console.info('Nav - Mobile external button loaded');
                }

                clearInterval(hive_cpt_nav_mobile_buttonHamburger__interval);

                $('button#buttonHamburger').on('click', function(event){
                    // Avoid following the href location when clicking
                    event.preventDefault();
                    // Avoid having the menu to close when clicking
                    event.stopPropagation();
                    $('.hamburger button').click();
                });

            }

        }

    }

}, 1000);